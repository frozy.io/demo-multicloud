# Configure AWS Provider
provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "${var.region}"
}

# Configure GCP Provider
provider "google" {
  credentials = "${file("${var.GCP_SVCJSONFILE}")}"
  project = "${var.GCP_PROJECT}"
  region = "${var.GCP_REGION}"
}
