output "frozy-consumer_ipaddress" {
  value = "${aws_lightsail_instance.frozy-consumer.public_ip_address}"
}

output "frozy-provider_ipaddress" {
  value = "${google_compute_instance.frozy-provider.0.network_interface.0.access_config.0.assigned_nat_ip}"
}

output "frozy_demo_browser_access" {
  value = "Please use the following URL to access Demo: http://${aws_lightsail_instance.frozy-consumer.public_ip_address}"
}
