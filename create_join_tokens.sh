#!/bin/bash
# Input can be supplied either via environment variables or by sending JSON to
# stdin (see below for description)
#
# Required inputs:
#  * FROZY_RESOURCE_NAME - name of the resource to register
#  * FROZY_ACCESS_TOKEN - access token to use for backend authentication
#
# Optional inputs:
#  * FROZY_CONSUMER_PORT - If set then Consumer Join Token would be created
#     (value of this variable would be used as a consumer port)
#
#  * FROZY_PROVIDER_HOST - If set then Provider Join Token would be created
#     (value of this variable would be used as a provider host)
#  * FROZY_PROVIDER_PORT - If set then Provider Join Token would be created
#     (value of this variable would be used as a provider port)
#
#  * FROZY_TIER - Tier to use (sandbox/dev/demo/pilot). Ignored if
#     FROZY_BACKEND_URL is set.
#  * FROZY_BACKEND_URL - URL for accessing backend. If this is set then
#     FROZY_TIER is ignored
#  * FROZY_FAIL_IF_EXISTS - If set to anything then registration process
#     will fail when resource with FROZY_RESOURCE_NAME already exists
#
#  When using JSON to stdin for passing parameters, mapping of JSON
# FROZY_RESOURCE_NAME  <-> resourceName
# FROZY_ACCESS_TOKEN   <-> accessToken
# FROZY_CONSUMER_PORT  <-> consumerPort
# FROZY_PROVIDER_HOST  <-> providerHost
# FROZY_PROVIDER_PORT  <-> providerPort
# FROZY_TIER           <-> tier
# FROZY_BACKEND_URL    <-> backendUrl
# FROZY_FAIL_IF_EXISTS <-> failIfExists

read -t 1 input
if [ ! -z "$input" ]; then
  eval "$(echo "$input" | jq -r '@sh "FROZY_RESOURCE_NAME=\(.resourceName // empty)"')"
  eval "$(echo "$input" | jq -r '@sh "FROZY_ACCESS_TOKEN=\(.accessToken // empty)"')"
  eval "$(echo "$input" | jq -r '@sh "FROZY_CONSUMER_PORT=\(.consumerPort // empty)"')"
  eval "$(echo "$input" | jq -r '@sh "FROZY_PROVIDER_HOST=\(.providerHost // empty)"')"
  eval "$(echo "$input" | jq -r '@sh "FROZY_PROVIDER_PORT=\(.providerPort // empty)"')"
  eval "$(echo "$input" | jq -r '@sh "FROZY_TIER=\(.tier // empty)"')"
  eval "$(echo "$input" | jq -r '@sh "FROZY_BACKEND_URL=\(.backendUrl // empty)"')"
  eval "$(echo "$input" | jq -r '@sh "FROZY_FAIL_IF_EXISTS=\(.failIfExists // empty)"')"
  (set -o posix ; set) | grep FROZY >&2
fi

set -e

if [ -z "$FROZY_RESOURCE_NAME" ]; then
  echo "Please set FROZY_RESOURCE_NAME environment variable to name of the " >&2
  echo "resource you want to create" >&2
  exit 1
fi


if [ -z "$FROZY_ACCESS_TOKEN" ]; then
  echo "Please set FROZY_ACCESS_TOKEN environment variable to an access token " >&2
  echo "you obtained from Frozy frontend" >&2
  exit 1
fi

if [ -z "$FROZY_TIER" ]; then
  FROZY_BACKEND_URL="${FROZY_BACKEND_URL:-https://frozy.cloud/api/v1}"
else
  FROZY_BACKEND_URL="${FROZY_BACKEND_URL:-https://$FROZY_TIER.frozy.cloud/api/v1}"
fi

echo "Using backend at $FROZY_BACKEND_URL" >&2

# Sets http_response to HTTP response, sets $http_code to HTTP status code
function makeRequest() {
  http_code=0
  if [ ! -z "$FROZY_INSECURE" ]; then
    curl_additionals="--insecure "
  fi

  tmp_response=$(mktemp)

  http_code=$(curl -H "X-Access-Token: $FROZY_ACCESS_TOKEN" -s -o "$tmp_response" -w '%{http_code}' -v $curl_additionals "$@")

  http_response=$(cat "$tmp_response")
  rm "$tmp_response"
}

function validateHttpCode() {
  if [ "$1" -eq 0 ]; then
    cat <<EOM >&2
Couldn't connect to Backend. Are you using insecure tier (sandbox)
without FROZY_INSECURE environment variable set to "yes"?
EOM
    return 1
  elif [ "$1" -eq 401 ]; then
    cat <<EOM >&2
401 - $2
Most likely access Token is not valid. Try obtaining it from the frontend once
again.
EOM
    return 1
  elif [ ! "$1" -eq 200 ]; then
    echo "Backend returned HTTP $1 - $2" >&2
    return 1
  fi

  return 0
}

makeRequest -X GET "$FROZY_BACKEND_URL/info"
validateHttpCode $http_code "$http_response"
if [ ! $? -eq 0 ]; then exit 1; fi

echo "Info: $http_response" >&2
farm_id=$(echo $http_response | jq -r ".FarmID")
if [ ! $? -eq 0 ]; then exit 1; fi
echo "FarmID: $farm_id" >&2

create_resource_url="$FROZY_BACKEND_URL/farm/$farm_id/resources/$FROZY_RESOURCE_NAME"
echo "Create resource url: $create_resource_url" >&2
makeRequest -X PUT "$create_resource_url"
if [ "$http_code" -eq 409 ]; then
  if [ -z "$FROZY_FAIL_IF_EXISTS" ]; then
    : # Resource already exists, but that's fine. Do nothing.
  else
    echo "Resource \"$FROZY_RESOURCE_NAME\" already exists." >&2
  fi
else
  validateHttpCode $http_code "$http_response"
  if [ ! $? -eq 0 ]; then exit 1; fi
fi

create_token_url="$FROZY_BACKEND_URL/farm/$farm_id/resources/$FROZY_RESOURCE_NAME/tokens"
if [ ! -z "$FROZY_CONSUMER_PORT" ]; then
  echo "Generating consumer token for port $FROZY_CONSUMER_PORT" >&2
  payload="{\"role\": \"consumer\", \"port\": \"$FROZY_CONSUMER_PORT\"}"
  echo "$payload" >&2
  makeRequest -X POST -d "$payload" "$create_token_url"
  validateHttpCode $http_code "$http_response"
  if [ ! $? -eq 0 ]; then exit 1; fi
  CONSUMER_JOIN_TOKEN="$http_response"
  echo "Consumer Join Token: $CONSUMER_JOIN_TOKEN" >&2
fi

if [ ! -z "$FROZY_PROVIDER_PORT" ] && [ ! -z "$FROZY_PROVIDER_HOST" ]; then
  echo "Generating provider token for port $FROZY_PROVIDER_PORT and host $FROZY_PROVIDER_HOST" >&2
  payload="{\"role\": \"provider\", \"port\": \"$FROZY_PROVIDER_PORT\", \"host\": \"$FROZY_PROVIDER_HOST\"}"
  echo "$payload" >&2
  makeRequest -X POST -d "$payload" "$create_token_url"
  validateHttpCode $http_code "$http_response"
  if [ ! $? -eq 0 ]; then exit 1; fi
  PROVIDER_JOIN_TOKEN="$http_response"
  echo "Provider Join Token: $PROVIDER_JOIN_TOKEN" >&2
fi

jq -n --arg consumerJoinToken "$CONSUMER_JOIN_TOKEN" \
   --arg providerJoinToken "$PROVIDER_JOIN_TOKEN" \
   '{"consumerJoinToken": $consumerJoinToken, "providerJoinToken": $providerJoinToken}'
