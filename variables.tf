# AWS account data
variable "aws_access_key" {
  description = "AWS Access Key"
  default = "_put_access_key_here_"
}

variable "aws_secret_key" {
  description = "AWS Secret Key"
  default = "_put_secret_key_here_"
}

variable "region" {
  description = "Region that the instances will be created"
  default = "us-west-2"
}

# GCP account data
variable "GCP_REGION" {
  default = "us-west1-b"
}
variable "GCP_PROJECT" {
  default = "_gcp_project_id__"
}
variable "GCP_SVCJSONFILE" {
  default = "_gcp_secret_key_file_.json"
}

# Frozy tier and access token
variable "frozy_tier" {
  description = "Frozy Environment Tier Name"
  default = "demo"
}
variable "frozy_access_token" {
  description = "Access Token for Frozy that you have obtained"
  default = "_put_frozy_access_token_here_"
}
