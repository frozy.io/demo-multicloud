# Demo environment with Terraform Deployment for Frozy Cloud

## Terraform host requirements
- `terraform` version >= v0.11.9
- `jq` tool installed for JSON manupulations

## Cloud access rights requirements
1. GCP service account should have at least `Compute Instance Admin (v1)` or `Compute Admin` role applied.

Compute Engine API should be enabled here https://console.cloud.google.com/apis/library/compute.googleapis.com

2. Amazon AWS user (API key) requires Custom policy applied for AWS Lightsail access:
```
{
 "Version": "2012-10-17",
 "Statement": [
   {
     "Effect": "Allow",
     "Action": [
       "lightsail:*"
     ],
     "Resource": "*"
   }
 ]
}
```
Detailed information can be found here:

https://lightsail.aws.amazon.com/ls/docs/en/articles/create-policy-that-grants-access-to-amazon-lightsail

## Initial setup

1. Clone the repository
```
git clone https://gitlab.com/frozy.io/demo-multicloud.git
cd demo-multicloud
```

2. Obtain Frozy Access Token

- Login here https://demo.frozy.cloud/

- Obtain Access Token here https://demo.frozy.cloud/access_token

3. Edit `variables.tf` and fill it with your GCP, AWS and Frozy secret keys, credentials and tokens.

## We are ready to go
Use the following commands to make it up and running:
```
  terraform init
  terraform plan
  terraform apply
```

## Deployment result
Consumer application and connector will be deployed to AWS Lightsail, this is PostgreSQL Web Client.

Provider application and connector will be deployed to Google Compute Engine, this is PostgreSQL Database.

Terraform output will show IP addresses of frozy-consumer and frozy-provider.

## Schema
There is following connection schema after deployment:

  (AWS)(frozy-consumer) http:80 -> consumer:5432 -> [FROZY CLOUD] <- provider -> postresql:172.17.0.1:5432 (frozy-provider){GCP)

Open in your Web browser (output from Terraform):

  `http://frozy-consumer_ipaddress`

the same as `frozy_demo_browser_access` infomation string at TF output.

Following credentinal should be used for Demo:
```
  user: frozy
  pass: frozy
```

## That's it!
You are connected via Frozy Cloud! You may browse remote database and run SQL queries.

## terraform destroy
Do not forget to destroy Demo Environment after play to save your resources and your money. 
