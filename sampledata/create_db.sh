#!/bin/bash

dropdb --if-exists frozy
dropuser --if-exists frozy

createdb frozy
psql frozy < northwind.sql

psql template1 -c "create user frozy;"
psql template1 -c "alter user frozy password 'frozy';"
psql template1 -c "grant all on DATABASE frozy to frozy;"
psql frozy -c "GRANT ALL on ALL tables IN SCHEMA public to frozy"
