# Prepare temporary SSH keys
resource "tls_private_key" "ssh" {
  algorithm = "RSA"
}
resource "local_file" "id_rsa" {
  content     = "${tls_private_key.ssh.private_key_pem}"
  filename = ".id_rsa"
  depends_on = ["tls_private_key.ssh"]
}
resource "local_file" "id_rsa_pub" {
  content     = "${tls_private_key.ssh.public_key_openssh}"
  filename = ".id_rsa.pub"
  depends_on = ["tls_private_key.ssh"]
}
resource "null_resource" "id_rsa_set_permissions" {
  provisioner "local-exec" {
    command = "chmod 600 .id_rsa && chmod 644 .id_rsa.pub"
  }
  depends_on = [
    "local_file.id_rsa",
    "local_file.id_rsa_pub",
  ]
  triggers = {
    ssh_keys = "${tls_private_key.ssh.private_key_pem}"
  }
}

# Obtain join tokens for 2 resources
data "external" "join_tokens" {
  program = ["${path.module}/create_join_tokens.sh"]

  query = {
    tier         = "${var.frozy_tier}"
    accessToken  = "${var.frozy_access_token}"
    resourceName = "demo-multicloud"
    consumerPort = 5432
    providerPort = 5432
    providerHost = "172.17.0.1"
  }
}

# Create Amazon Lightsail resources
resource "aws_lightsail_key_pair" "frozy_keypair" {
  name   = "frozykeypair"
  public_key = "${tls_private_key.ssh.public_key_openssh}"
}

resource "aws_lightsail_instance" "frozy-consumer" {
  name              = "frozy-consumer"
  availability_zone = "us-west-2b"
  blueprint_id      = "ubuntu_18_04"
  bundle_id         = "nano_1_0"
  key_pair_name     = "frozykeypair"

  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      host        = "${self.public_ip_address}"
      private_key = "${tls_private_key.ssh.private_key_pem}"
      user        = "ubuntu"
    }
    inline = [
	# update clean environment
        "sudo apt-get update",
        "sudo DEBIAN_FRONTEND=noninteractive apt-get -y upgrade",

	# install latest docker release
        "sudo apt-get -y install apt-transport-https",
        "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -",
        "sudo apt-key fingerprint 0EBFCD88",
        "sudo add-apt-repository \"deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable\"",
        "sudo apt-get update",
        "sudo apt-get install -y docker-ce",
        "sudo usermod -aG docker ubuntu",

	# frozy connector
        "sudo docker run -d --restart unless-stopped --net=host --env FROZY_JOIN_TOKEN=${data.external.join_tokens.result["consumerJoinToken"]} --name C-${data.external.join_tokens.result["consumerJoinToken"]} --env FROZY_TIER=${var.frozy_tier} frozy/connector:0.1",

	# application
	"sudo docker run -d -p 80:80 --name=teampostgresql --env TEAMPOSTGRESQL_ADMIN_USER=frozy --env TEAMPOSTGRESQL_ADMIN_PASSWORD=frozy --env TEAMPOSTGRESQL_DEFAULT_HOST=172.17.0.1 --env TEAMPOSTGRESQL_DEFAULT_USERNAME=frozy --env TEAMPOSTGRESQL_DEFAULT_PASSWORD=frozy --env TEAMPOSTGRESQL_DEFAULT_DATABASENAME=frozy frozydemo/teampostgresql",
    ]
  }
}

# Create a new instance
resource "google_compute_instance" "frozy-provider" {
  count = 1
  name = "frozy-provider"
  machine_type = "f1-micro"
  zone = "us-west1-b"
  boot_disk {
    initialize_params {
      image = "ubuntu-1804-lts"
    }
  }

  network_interface {
    network = "default"
    access_config {}
  }

  metadata {
    sshKeys = "ubuntu:${tls_private_key.ssh.public_key_openssh}"
  }

  provisioner "file" {
    source      = "sampledata/"
    destination = "/tmp"

    connection {
      type        = "ssh"
      private_key = "${tls_private_key.ssh.private_key_pem}"
      user = "ubuntu"
    }
  }

  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      private_key = "${tls_private_key.ssh.private_key_pem}"
      user = "ubuntu"
    }
    inline = [
	# update clean environment
        "sudo apt-get update",
        "sudo DEBIAN_FRONTEND=noninteractive apt-get -y upgrade",

	# install latest docker release
        "sudo apt-get -y install apt-transport-https",
        "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -",
        "sudo apt-key fingerprint 0EBFCD88",
        "sudo add-apt-repository \"deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable\"",
        "sudo apt-get update",
        "sudo apt-get install -y docker-ce",
        "sudo usermod -aG docker ubuntu",

	# frozy connector
        "sudo docker run -d --restart unless-stopped --net=host --env FROZY_JOIN_TOKEN=${data.external.join_tokens.result["providerJoinToken"]} --name P-${data.external.join_tokens.result["providerJoinToken"]} --env FROZY_TIER=${var.frozy_tier} frozy/connector:0.1",

	# application
	"sudo docker run -d --restart unless-stopped --name=postgresql -p 172.17.0.1:5432:5432 -v /tmp:/tmp2:ro postgres:10.6-alpine",
	"sudo chmod +x /tmp/create_db.sh",
	"nohup sleep 30",
	"sudo docker exec -it -u postgres postgresql bash -c 'cd /tmp2 && /tmp2/create_db.sh'",
    ]
  }
}
